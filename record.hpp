#ifndef CAESAR_HPP_
#define CAESAR_HPP_ 

#include <ctime>
#include <fstream>
#include <string>
#include <vector>

class Record {

public: 
		Record();
		virtual ~Record();

		//Return a string of tweet ID's that need posting
		virtual std::vector<std::string> getNextTweets();

		//Takes in a string and sets the filename
		virtual void setFileName( std::string new_name );

private:
		std::string fileName = "tweetSchedule.txt";
};

#endif