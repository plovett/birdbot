#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "record.hpp"

Record::Record() {

}

Record::~Record() {

}

void Record::setFileName( std::string new_name ){
	this->fileName = new_name;
}

std::vector<std::string> Record::getNextTweets(){
	std::fstream myFile;
	std::string line;
	std::time_t now = std::time(0);
	std::vector<std::string> tweetsToSend;
	myFile.open( this->fileName );
	printf("Looking throught the file now\n");


	while( getline (myFile,line) ){
		if(line == "\n"){
			break;
		}
		struct tm tweetTime;

		tweetTime.tm_sec = std::stoi( line.substr(17,2) );
		tweetTime.tm_min = std::stoi( line.substr(14,2) );
		tweetTime.tm_hour =std::stoi( line.substr(11,2) );
		tweetTime.tm_mday =std::stoi( line.substr(3,2) );
		tweetTime.tm_mon  =std::stoi( line.substr(0,2) )-1;
		tweetTime.tm_year =std::stoi( line.substr(6,4) )-1900;

		double time_diff = difftime( now, mktime(&tweetTime));
		if((time_diff > 0) && (time_diff < 600 )){
			
			tweetsToSend.push_back( line.substr(20) );
			printf("Found a tweet to send\n");
		}

	}

	myFile.close();
	printf("Just about to return a tweet list\n");
	return tweetsToSend;
}

