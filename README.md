# Tweet in C++

Schedule Tweets with this neat app. BirdBot allows for scheduling tweets using a shell interface. Tweets queued to send are checked every 10 minutes.

## Before Starting - Create a Twitter App for your Bot

Go to Twitter.com and sign up for a new account if you haven't already, then create a twitter App for your Bot.

Go to apps.twitter.com and hit the “Create New App” button there. Fill out the form to give your app a name, description, and website. These can be quite simple and can always be changed later. The app’s name needs to be unique. Leave the “Callback” field blank for now. If you get an Error message saying that you must first add a mobile phone number to your profile, then you should do that now.

Under Details, make sure that the app’s access level is set to “Read and Write”, and if not, change that under the Permissions tab.

Under the Keys and Access Tokens tab, use the button to “Create my access token.” This will authorize your app to interface with your account.

Then, copy and paste the Access Token into a file named "BirdBot_token_key.txt" in the same directory as the repo and Access Token Secret into a file named "BirdBot_token_secret.txt" also in the same directory. This also must be done for the Consumer Key and Consumer Secret Key for their respective file names listed below.

This can be done via the shell with the following commands:

```$ cd birdbot```

```$ echo pasteYourConsumerKeyHere >> secrets/BirdBot_consumer_key.txt```

```$ echo pasteYourConsumerSecretHere >> secrets/BirdBot_consumer_secret.txt```

```$ echo pasteYourTokenKeyHere >> secrets/BirdBot_token_key.txt```

```$ echo pasteYourTokenSecretHere >> secrets/BirdBot_token_secret.txt```

Make sure to setup crontab, which is used to schedule tweets. Simply open it once to setup by typing the following:

```$ crontab -e```

It may ask you for a preferred text editor, but afterward quit your text editor.

##Required Libraries

* OAuth
* libcurl-dev
* twitcurl (https://github.com/swatkat/twitcurl/)
* crontab

For instructions to build and use the twitcurl library, see:
https://code.google.com/archive/p/twitcurl/wikis/WikiHowToUseTwitcurlLibrary.wiki

Our repo contains a version of twitcurl with an edited Makefile which should build when make is run inside the birdbot folder.

##Installation and Usage

After pulling the repo, from the directory you pulled into:

```$ cd birdbot```

```$ make```

or depending on the privileges you've given the libcurl package,

```$ sudo make```


To add a Tweet to your schedule, run the following and you'll be walked through a CLI to produce a tweet:

```$ ./TweetScheduler```

Afterward, you can run BirdBot with the following syntax from the repo directory:

```$ schedule```

Note: Do not run ```schedule``` again unless you clear the last entry using `crontab -e`. It is also recommended that you remove birdbot from your crontab if you plan to not use the bot for an extended period of time then running ```schedule``` again.

## Troubleshooting

Information about the status of sent tweets will be sent to ```cronlog.log```.

You may check the real time scheduling of queued tweets from the system by typing the following:

```$ tail -f cronlog.log```

Check here for errors if your tweets are not sending.

You may also manually send tweets via BirdBot by running:

```$ ./BirdBot```

Before doing so, make sure your Library path for libtwitcurl is configured correctly. You may change this by typing the following in the birdbot directory:

```$ echo $LD_LIBRARY_PATH```

If this returns a blank line in the shell, try this:

```$ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/birdbot/libtwitcurl.so.1```
```$ export LD_LIBRARY_PATH```