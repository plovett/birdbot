#include "BirdBot.hpp"
#include "record.hpp"

int main( int argc, char* argv[] )
{

    twitCurl twitterObj;
    std::string tmpStr, tmpStr2;
    std::string replyMsg;
    char tmpBuf[1024];

    /* OAuth flow begins */
    /* Step 0: Set OAuth related params. These are got by registering your app at twitter.com */

    std::string myConsumerKey("");
    std::string myConsumerSecret("");
    std::ifstream consumerKeyIn;
    std::ifstream consumerSecretIn;

    consumerKeyIn.open( "secrets/BirdBot_consumer_key.txt" );   
    consumerSecretIn.open( "secrets/BirdBot_consumer_secret.txt" );

    memset( tmpBuf, 0, 1024 );
    consumerKeyIn >> tmpBuf;
    myConsumerKey = tmpBuf;

    memset( tmpBuf, 0, 1024 );
    consumerSecretIn >> tmpBuf;
    myConsumerSecret = tmpBuf;

    twitterObj.getOAuth().setConsumerKey( std::string(myConsumerKey) );
    twitterObj.getOAuth().setConsumerSecret( std::string(myConsumerSecret) );

    consumerKeyIn.close();
    consumerSecretIn.close();

    /* Step 1: Check if we alredy have OAuth access token from a previous run */
    std::string myOAuthAccessTokenKey("");
    std::string myOAuthAccessTokenSecret("");
    std::ifstream oAuthTokenKeyIn;
    std::ifstream oAuthTokenSecretIn;

    //TODO sperate credential file function
    oAuthTokenKeyIn.open( "secrets/BirdBot_token_key.txt" );
    oAuthTokenSecretIn.open( "secrets/BirdBot_token_secret.txt" );

    memset( tmpBuf, 0, 1024 );
    oAuthTokenKeyIn >> tmpBuf;
    myOAuthAccessTokenKey = tmpBuf;

    memset( tmpBuf, 0, 1024 );
    oAuthTokenSecretIn >> tmpBuf;
    myOAuthAccessTokenSecret = tmpBuf;

    oAuthTokenKeyIn.close();
    oAuthTokenSecretIn.close();

    if( myOAuthAccessTokenKey.size() && myOAuthAccessTokenSecret.size() )
    {
        /* If we already have these keys, then no need to go through auth again */
        printf( "\nUsing:\nKey: %s\nSecret: %s\n\n", myOAuthAccessTokenKey.c_str(), myOAuthAccessTokenSecret.c_str() );

        twitterObj.getOAuth().setOAuthTokenKey( myOAuthAccessTokenKey );
        twitterObj.getOAuth().setOAuthTokenSecret( myOAuthAccessTokenSecret );
    }
    else
    {
        /* Step 2: Get request token key and secret */
        std::string authUrl;
        twitterObj.oAuthRequestToken( authUrl );

        /* Step 3: Get PIN  */
        memset( tmpBuf, 0, 1024 );
        printf( "\nDo you want to visit twitter.com for PIN (0 for no; 1 for yes): " );
        fgets( tmpBuf, sizeof( tmpBuf ), stdin );
        tmpStr = tmpBuf;
        if( std::string::npos != tmpStr.find( "1" ) )
        {
            /* Ask user to visit twitter.com auth page and get PIN */
            memset( tmpBuf, 0, 1024 );
            printf( "\nPlease visit this link in web browser and authorize this application:\n%s", authUrl.c_str() );
            printf( "\nEnter the PIN provided by twitter: " );
            fgets( tmpBuf, sizeof( tmpBuf ), stdin );
            tmpStr = tmpBuf;
            twitterObj.getOAuth().setOAuthPin( tmpStr );
        }
        else
        {
            /* Else, pass auth url to twitCurl and get it via twitCurl PIN handling */
            twitterObj.oAuthHandlePIN( authUrl );
        }

        /* Step 4: Exchange request token with access token */
        twitterObj.oAuthAccessToken();

        /* Step 5: Now, save this access token key and secret for future use without PIN */
        twitterObj.getOAuth().getOAuthTokenKey( myOAuthAccessTokenKey );
        twitterObj.getOAuth().getOAuthTokenSecret( myOAuthAccessTokenSecret );

        /* Step 6: Save these keys in a file or wherever */
        std::ofstream oAuthTokenKeyOut;
        std::ofstream oAuthTokenSecretOut;

        oAuthTokenKeyOut.open( "twitterClient_token_key.txt" );
        oAuthTokenSecretOut.open( "twitterClient_token_secret.txt" );

        oAuthTokenKeyOut.clear();
        oAuthTokenSecretOut.clear();

        oAuthTokenKeyOut << myOAuthAccessTokenKey.c_str();
        oAuthTokenSecretOut << myOAuthAccessTokenSecret.c_str();

        oAuthTokenKeyOut.close();
        oAuthTokenSecretOut.close();
    }
    /* OAuth flow ends */

    /* Account credentials verification */
    if( twitterObj.accountVerifyCredGet() )
    {
        twitterObj.getLastWebResponse( replyMsg );
        printf( "ACCOUNT VERIFICATION: SUCCESS\n\n");
        try{
            printf( "\ntwitterClient:: twitCurl::accountVerifyCredGet web response:\n%s\n", replyMsg.c_str() );
        }catch (...){
            printf("\nCheck failed, ensure that your consumer key and consumer secret are properly populated\n");
        }
    }
    else
    {
        twitterObj.getLastCurlError( replyMsg );
        printf( "ACCOUNT VERIFICATION: FAILURE\n\n");
        printf( "\ntwitterClient:: twitCurl::accountVerifyCredGet error:\n%s\n", replyMsg.c_str() );
    }

    Record* myFile = new Record();
    std::vector<std::string> tweetList = (myFile)->getNextTweets();
    printf("Returning a list of tweets to send\n");
    unsigned int tweet_count = tweetList.size();
    replyMsg = "";

    for(unsigned i=0; i < tweet_count; i++){
        if( twitterObj.statusUpdate( tweetList[i] ) ){

            twitterObj.getLastWebResponse( replyMsg );
            printf("\ntwitterClient:: twitCurl:: statusUpdate web response:\n%s\n", replyMsg.c_str() );
            printf("A TWEET WAS POSTED HOUSTON\n");

        }else{

            twitterObj.getLastCurlError( replyMsg );
            printf("\ntwitterClient:: twitCurl::statusUpdate error:\n%s\n", replyMsg.c_str() );
        
        }
    }

  }
