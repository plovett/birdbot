//
//  BirdBot.hpp
//  BirdBot
//
//  Created by Meme on 2/20/17.
//  Copyright © 2017 CIS330. All rights reserved.
//

#ifndef BIRDBOT_HPP
#define BIRDBOT_HPP

#include <stdio.h>
#include <string>
#include "include/twitcurl.h"

using namespace std;


class BirdBot{
public:
    BirdBot();
    ~BirdBot();

    /* Create twitter service via OAuth, will probably not return void TODO */
    void getTwitterService();
    /* Check authorization, requires request param TODO */
    void authCallback();

    /* Get string text from file */
    std::string getSelectText();
    /* Generate tweet text from file */
    std::string generateSingleTweet();

    /* Schedule time next tweet will be posted */
    void scheduleTweet();
    /* Post a single tweet */
    void postTweet();

private:
    /* Strip unwanted characters from a tweet for request posting */
    std::string fixedEncodeURIComponent(std::string tweet);

};

#endif /* BirdBot_hpp */
