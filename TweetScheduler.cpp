#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;


int getAnInt(string userPrompt, int lowBound, int upperBound){
	int temp_number;
	string input = "";

	while(true){
		printf("\n%s", userPrompt.c_str());
		getline(cin, input);

		stringstream myStream(input);
		if((myStream >> temp_number) &&
			(temp_number <= upperBound && temp_number >= lowBound)){
			break;
		}
		printf("Invalid number, please try again");
	}
	return temp_number;
}


int main(){

	string nextTweet = "";
	int nextNumber;
	string input = "";
	int temp_number;
	string newNumber;

	printf("This tool will schedule a tweet for you.\n");

	//Month
	nextNumber = getAnInt("Please enter the month you want to post your tweet(1-12): ", 
		                  1, 12);
	newNumber = to_string(nextNumber);
	if(newNumber.length() <= 1){
		nextTweet += "0"+newNumber+"/";
	}else{
		nextTweet += newNumber+"/";
	}

	//Day
	nextNumber = getAnInt("Please enter the day you want to post your tweet(1-31): ", 
		                  1, 31);
	newNumber = to_string(nextNumber);
	if(newNumber.size() <= 1){
		nextTweet += "0"+newNumber+"/";
	}else{
		nextTweet += newNumber+"/";
	}

	//Year
	nextNumber = getAnInt("Please enter the year you want to post your tweet(0000-9999): ", 
		                  0, 9999);
	newNumber = to_string(nextNumber);
	if(newNumber.size() <= 1){
		nextTweet += "000"+newNumber+";";
	}else if(newNumber.size() == 2){
		nextTweet += "00" + newNumber+";";
	}else if(newNumber.size() == 3){
		nextTweet += "0" + newNumber+";";
	}else{
		nextTweet += newNumber+";";
	}

	//Hour
	nextNumber = getAnInt("Please enter the hour you want to post your tweet(00-23): ", 
		                  0, 23);
	newNumber = to_string(nextNumber);
	if(newNumber.size() <= 1){
		nextTweet += "0"+newNumber+":";
	}else{
		nextTweet += newNumber+":";
	}
	
	//Minute
	nextNumber = getAnInt("Please enter the minute you want to post your tweet(00-59): ", 
		                  0, 59);
	newNumber = to_string(nextNumber);
	if(newNumber.size() <= 1){
		nextTweet += "0"+newNumber+":00,";
	}else{
		nextTweet += newNumber+":00,";
	}

	//Tweet
	while(true){
		printf("\nPlease input the text of your tweet (140 char or less): ");
		getline(cin, input);
		if(input.size() <= 140 && input.size() > 0)
			break;
		printf("Too many characters, please try again");
	}
	nextTweet += input;

	ofstream tweetSchedule;
	tweetSchedule.open( "tweetSchedule.txt", std::ios::app );
	tweetSchedule << nextTweet << "\n";
	tweetSchedule.close();

	printf("Writing tweet file: \n%s\n", nextTweet.c_str());

}
