CC=g++ -std=c++11

all : BirdBot TweetScheduler

BirdBot : BirdBot.cpp 
	cd libtwitcurl && make clean && make install
	cp libtwitcurl/libtwitcurl.so.1.0 libtwitcurl.so.1
	$(CC) BirdBot.cpp record.cpp record.hpp libtwitcurl.so.1 -o BirdBot

TweetScheduler:
	$(CC) TweetScheduler.cpp -o TweetScheduler


clean :
	rm BirdBot
	rm TweetScheduler
	rm libtwitcurl.so.1
	cd ../birdbot
